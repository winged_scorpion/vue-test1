import ApiService from "@/service/api.js"

export const profileModule = {
    state() {
        return {
            profileArr: [],
            modalVisible: false,
            itemProfile: [],
            getStatus: 'Стартуем ?',
            profile: [],
            modalSlot: [],
            isButtonDisabled: false
        }
    },
    mutations: {
        setItemProfile(state, profile) {
            state.itemProfile = profile;
        },
        setModalAction(state) {
            state.modalVisible = !state.modalVisible;
        },
        getProfile(state, size) {
            ApiService.getProfile(size).then((response) => {
                state.profileArr = response;
                state.getStatus = 'Успешно';
                state.isButtonDisabled = false;
            }).catch(() => {
                state.getStatus = 'Глючный сервис, перезагрузи';
                state.isButtonDisabled = false;
            })
            state.getStatus = 'Ждём';
        },
        disableButton(state) {
            state.isButtonDisabled = true;
        }
    },
    getters: {},
    actions: {
        modalAction(context) {
            context.commit('setModalAction');
        },
        loadProfile(context, size) {
            context.commit('getProfile', size);
            context.commit('disableButton');
        },
        postItemProfile(context, item) {
            context.commit('setItemProfile', item);
        }
    }
}


