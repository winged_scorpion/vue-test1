import {createStore} from "vuex";
import {profileModule} from "@/store/profileModule";


export default createStore({
    modules: {
        post: profileModule
    }
})