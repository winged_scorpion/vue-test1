import axios from "axios";

export default class ApiService {
    static getProfile(size) {
        return axios
            .get('https://randomuser.me/api/?results=' + size)
            .then(response => {
                return response.data.results.map(function (task, index) {
                    return {
                        id: index,
                        name: task.name.title + task.name.first + task.name.last,
                        phone: task.phone,
                        gender: task.gender,
                        picture: task.picture.thumbnail,
                        email: task.email
                    }
                });
            })

    }
}
